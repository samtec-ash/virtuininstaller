
$DIR="$PSScriptRoot"
$USER="$(whoami)"
$PYTHON_PATH="C:\Python27"
$VIRTUIN_PATH="$DIR"
$VIRT_BROKER_ADDRESS="localhost"
$VAGRANT_CWD="$VIRTUIN_PATH/box"
$VAGRANT_TASK_PATH="$VAGRANT_CWD/vagrantUpTask.xml"
$VIRTUIN_GUI_INSTALL_PATH="$VIRTUIN_PATH/bin/VirtuinCoreSetup.exe"

# Install chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
$env:ChocolateyInstall = Convert-Path "$((Get-Command choco).path)\..\.."
Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
RefreshEnv.cmd

# Install base packages
choco install -y git --version 2.17.0
choco install -y docker --version 17.0.0
choco install -y docker-compose --version 1.20.1
choco install -y docker-machine --version 0.14.0
choco install -y virtualbox --version 5.2.8
choco install -y vagrant --version 2.0.3
choco install -y python2 --version 2.7.9 --params '"/InstallDir:C:\Python27"'
choco install -y atom
choco install -y visualstudiocode
RefreshEnv.cmd

# Set user env vars
setx.exe COMPOSE_FORCE_WINDOWS_HOST 0
setx.exe COMPOSE_CONVERT_WINDOWS_PATHS 1
setx.exe VIRT_PATH "$VIRTUIN_PATH"
setx.exe VIRT_BROKER_ADDRESS "$VIRT_BROKER_ADDRESS"
setx.exe VIRT_STATION_NAME "$COMPUTERNAME"
RefreshEnv.cmd

# Install/provision Virtuin VM
vagrant plugin install vagrant-vbguest
Push-Location $VAGRANT_CWD
# vagrant login --token "$VAGRANT_TOKEN"
vagrant up --provider virtualbox --machine-readable
vagrant status --machine-readable
Pop-Location

# Install task scheduler to start VM
schtasks /create /RU "$USER" /RP "*" /TN "Virtuin VM Boot" /XML "$VAGRANT_TASK_PATH" /F

# Install Virtuin GUI
Start-Process -FilePath $VIRTUIN_GUI_INSTALL_PATH
