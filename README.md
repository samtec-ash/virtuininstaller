# Anduin+Virtuin on Windows Setup

## Prerequisites

Install latest NI-VISA (17.0)

## Anduin Install

Run the following in an **elevated** powershell:
```powershell
\\samtec.ad\opticsmanufacturing\Misc\AnduinSetup32.exe
```

## Anduin Update

Run the following in an **elevated** powershell:
```powershell
\\samtec.ad\opticsmanufacturing\Configuration\AnduinUpdate\Anduin\AnduinSync-MSSQL.bat
```

## Virtuin Install

Run the following in an **elevated (admin)** powershell:
```powershell
./install.ps1
```

## Virtuin Update

**TODO**

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/samteccmd/virtuininstaller/commits/).

## Authors

* **Adam Page**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
